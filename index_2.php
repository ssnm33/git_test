<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>fadein test</title>
  <link rel="stylesheet" href="css/normalize.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/style_2.css">

  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script type="text/javascript" src="js/script_2.js"></script>
</head>
<body>
  <div id="preloader"><img src="http://www.loft-i.co.jp/common/images/header_logo.png" alt=""></div>
  <div id="is_loaded" class="l_wrapper header_first">
    <header class="">
      <!--インクルード virtual="/header.html"-->
      <h1>test</h1>
    </header>
    <section>
      <div class="content fadeInDown">
        <img src="images/img01.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img02.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img03.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img04.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img05.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img01.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img02.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img03.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img04.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img05.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img01.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img02.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img03.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img04.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img05.jpg" alt="">
      </div>
    </section>
  </div><!-- //l_wrapper -->
</body>
</html>
