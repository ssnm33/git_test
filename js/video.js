$(function() {

  var e = $(window).height();
  $('.hero').height(e);
  $(".hero").css("lineHeight",e + "px");

  $(window).resize(function(){
    var e = $(window).height();
    $('.hero').height(e);
    $(".hero").css("lineHeight",e + "px");
  });

  var wrap = $('#is_loaded');

  $(window).scroll(function(){
    var scrollY = $(this).scrollTop(),
        headp = 3;

    if(scrollY > headp){
      wrap.removeClass('header_first');
    } else if (scrollY < headp) {
      wrap.addClass('header_first');
    }

  });

  $(window).scroll(function(){
    var windowH = $(window).height(),
        topWindow = $(window).scrollTop();
    $('header').each(function(){
      var targetPosition = $(this).offset().top;
      if(topWindow > targetPosition - windowH + 200){
        $(this).addClass('fadeInDown');
      }
    })
  });


});
